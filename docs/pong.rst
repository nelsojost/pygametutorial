Pygame - Pong!
==============

Acredito que a melhor maneira de conhecer uma biblioteca é através de um bom exemplo. Aqui vamos construir o clássico jogo Pong! utilizando Python + Pygame onde o foco é obter um joguinho simples mas funcional, com jogabilidade suave e assim trazendo à tona alguns dos principais conceitos de Pygame (e até mesmo game development em geral). Sem mais, mãos à obra!

:mod:`1 - Programa mínimo`
--------------------------

Eis o código mínimo para um programa em Pygame exibir uma janela e ser capaz de encerrar adequadamente::

    import pygame, sys

    # carrega todos os modulos de pygame (video, som, fontes, etc)
    pygame.init()

    # objeto pygame.Surface que contem o conteudo a ser exibido no monitor
    screen = pygame.display.set_mode((400, 300))
       
    # --- main loop --- trata eventos, atualiza estado do jogo, desenha coisas na tela
    while True:
        
        for event in pygame.event.get():    # tratamento de eventos

            if event.type == pygame.QUIT:
                pygame.quit()       # desfaz pygame.init() e destroi a janela
                sys.exit()          # encerra o interpretador python

        pygame.display.update()     # atualiza a imagem no monitor
        
Com isso teremos uma simples tela preta, numa janela de tamanho 400 pixels de largura x 300 de altura. Essa janela foi criada (e exibida) ao se invocar ``pygame.display.set_mode()``, a qual também retorna um objeto ``pygame.Surface`` que representa um tipo especial de superfície: seu conteúdo será pintado no monitor sempre que ``pygame.display.update()`` for chamada. Por isso a superfície foi guardada numa variável chamada ``screen``.

Outra coisa importante que esse código mínimo introduz é o conceito de **main loop**, o "loop principal" do programa. Trata-se de um loop infinito onde três coisas acontecerão a cada iteração (e nessa ordem): 

* Tratamento de eventos (pressionamento de teclas, movimento/cliques do mouse, etc);
* Atualizações do estado do jogo (posição dos personagens, scores, etc);
* Desenhos na tela;

Quando um evento acontece, Pygame cria um objeto ``pygame.Event`` e o adiciona à uma certa fila corrente. A função ``pygame.event.get()`` esvazia essa fila, retornando os eventos numa lista. Tratar eventos significa então percorrer essa lista e testar pela ocorrência dos eventos que desejamos surtir algum efeito no programa. Aqui apenas o evento ``pygame.QUIT`` foi considerado: todo programa deve tratar pelo menos este, que deve ser responsável por chamar o código adequado para encerrar o programa.

Agora, perceba que o *main loop* estará sendo executado na velocidade máxima que a CPU permitir. Isso pode ser controlado com a introdução do objeto ``pygame.time.Clock``::

    ...    
    RESOLUTION, FRAMERATE = (400, 300), 200
    ...
    screen = pygame.display.set_mode(RESOLUTION)
    clock = pygame.time.Clock()
       
    while True:  # --- main loop ---
        ...
                
        # tempo que passou durante o quadro anterior (em segundos)
        dt = clock.tick(FRAMERATE)/1000.
        
        pygame.display.update()     # atualiza a imagem no monitor
       
OBS: Entenda ``...`` por "há código aqui que já foi mostrado/explicado". A partir de agora será mostrado apenas o novo código introduzido (juntamente com o local onde ele se encontra), que pressupõe tudo o que foi mostrado até então - a menos que se diga o contrário. A idéia é que você, leitor, digite o código por si mesmo e assim aprenda de maneira incremental.

O método ``tick()`` executa micropausas na execução do código, onde o programa literalmente dorme por frações de segundo. O parâmetro ``FRAMERATE`` indica a taxa de *quadros por segundo* em que o programa funcionará - ou seja, quantas vezes por segundo o *main loop* será executado. Aqui foi passado o valor 200 FPS ("frames per second") - logo mais discutiremos as implicações desse valor quando falarmos de animações. Por fim, ``tick()`` também retorna o tempo que passou durante esta e a última chamada dela mesma - em outras palavras, o tempo (em millisegundos) que passou durante um quadro. Esse valor foi convertido para segundos e guardado em ``dt`` para uso na criação de animações.

:mod:`2 - Superfícies, cores e animações - o básico`
----------------------------------------------------

Para fins de simplicidade, vamos lembrar dos velhos tempos do Atari onde a bola do Pong! era um quadrado que se movia na tela. Nosso objetivo nesta seção será então criar esse quadrado que se movimenta na tela, ricocheteando nas bordas.

Vamos começar capturando algumas informações sobre a bola em constantes globais (estas ficarão na parte superior do código para facilitar a manutenção)::

    import pygame, sys
    from pygame.locals import *

    RESOLUTION, FRAMERATE = (400, 300), 200
    BACKGROUND_COLOR = Color('black')
    
    BALL_DIAMETER, BALL_COLOR = 20, (255, 255, 255)     # diametro (em pixels)
    BALL_POS_0 = (100, 100)                             # posicao inicial
    ...

O sistema de coordenadas em Pygame possui o eixo x crescendo para a direita, e o eixo y crescendo para baixo. Isso se deve ao fato que em geral pensamos em termos de pixels num objeto ``pygame.Surface``, o qual pode ser visto como uma matriz de pixels numerados a partir do canto superior esquerdo. 

Falando em pixels, o sistema de cores em Pygame é o famoso RGBA (*Red/Green/Blue/Alpha*, ou ARGB como também é bastante conhecido). Nesse sistema todas as cores são formadas a partir de uma mistura de vermelho, verde e azul - consideradas *cores primárias*. Sim, o amarelo é cor primária quando se fala de pigmentos, mas no caso dos monitores de vídeo, fala-se de mistura de ondas de luz.

Assim, cada cor é dada simplesmente por uma tupla de três inteiros, variando de 0 a 255. Em ``BALL_COLOR`` temos configurada a cor branca. Um quarto valor - opcional, conhecido como *canal alpha* - pode ser fornecido para indicar o nível de transparência (também de 0 a 255). Por fim, antes de sair definindo constantes como ``BLACK = (0, 0, 0)`` saiba que Pygame oferece o objeto ``pygame.Color``, que é capaz de receber uma string com o nome de uma cor pré-definida. Todos os nomes válidos estão no dicionário ``pygame.color.THECOLORS``. O ideal aqui é fazer ``BALL_COLOR = Color('white')`` para facilitar a legibilidade.

Dito tudo isso podemos ir à criação da superfície da bola e o seu desenho na tela::

    ...
    ball_surf = pygame.Surface(2*[BALL_DIAMETER])
    ball_surf.fill(BALL_COLOR)
    ball_surf.convert()     # otimiza a superficie para blitagem

    ball_pos = list(BALL_POS_0)
    
    while True: # --- main loop ---
        ...
        
        # --- desenhos ---
        screen.fill(BACKGROUND_COLOR)
        screen.blit(ball_surf, ball_pos)

        pygame.display.update()     # atualiza a imagem no monitor
        
Na variável ``ball_surf`` temos então um objeto ``pygame.Surface`` de tamanho ``2*[BALL_DIAMETER]``, ou seja, ``[20, 20]``. Ao ser criada, uma superfície tem todos seus pixels com a cor preta. Pintamos então todo seu conteúdo com ``BALL_COLOR``, e invocamos ``convert()`` - garante que o formato de cores da superfície é o padrão utilizado em Pygame (isso é fundamental por exemplo, quando se carrega imagens externas, de formatos variados). Ainda fora do *main loop* também capturamos a posição da bola na variável ``ball_pos`` - deve ser uma lista justamente porque faremos a bola se movimentar.

Tudo até aqui foi feito na memória, e então o próximo passo é desenhar a bola na tela - ou seja, na superfície ``screen``. Como já temos uma superfície contendo o "desenho da bola" (futuramente, poderá ser uma imagem carregada de um arquivo externo), o que fazemos então é "blitar", ou simplesmente copiar, o conteúdo de ``ball_surf`` em ``screen``, numa dada posição. Repare no entanto que inauguramos uma nova seção de código para os desenhos, onde primeiro preenchemos o fundo da tela com ``BACKGROUND_COLOR`` e só então blitamos a superfície da bola (porquê?).

Temos então o terreno preparado para fazer a bola se mover. Como já temos a posição da bola (coordenadas x e y) numa lista, poderíamos simplesmente fazer::

    ...
    while True: # --- main loop ---
        ...
        ball_pos[0] += 1
        
        if ball_pos[0] > RESOLUTION[0]:   # mantem a bola dentro da tela
            ball_pos[0] = 0
        
        # --- desenhos ---
        ...

O que está acontecendo? Obviamente, a cada execução do *main loop* a coordenada x da bola está sendo incrementada em 1 pixel. Mas alguém poderia perguntar: e qual a velocidade da bola? Bom, considerando que a taxa de quadros por segundo é 200 FPS, e a cada quadro se incrementa 1 pixel, regra de três simples: 200 pixels/segundo. Há um porém, no entanto: isso não é bem verdade. A taxa de FPS de um jogo dificilmente se mantém constante. O valor configurado em ``FRAMERATE`` é um parâmetro usado por ``tick()`` para tentar manter a taxa constante - o programador, no entanto, certamente pode contar com pequenas variações ao longo da execução.

Esse tipo de animação, onde se fornece apenas o incremento de posição, é conhecido como *frame-based*, justamente porque a velocidade da animação depende exclusivamente da taxa real de quadros por segundo. Isso é ruim, pois além do que foi mencionado acima, pode-se desejar regular o parâmetro ``FRAMERATE`` do programa na tentativa de deixar a animação mais usave - o que faria também a velocidade dos objetos no jogo se alterarem. 

A solução é simplesmente usar o tempo real que passou entre os quadros para calcular o incremento de posição. Esse tempo está guardado na variável ``dt``, introduzida na seção anterior, graças ao objeto ``clock`` e seu método ``tick()``. Pois bem, introduzindo uma nova variável ``ball_speed`` para capturar a velocidade da bola, temos o que segue::
    
    ...
    # posicao inicial, velociade inicial (em pixels/segundo)
    BALL_POS_0, BALL_SPEED_0 = (100, 100), (100, 0)
    
    ...
    ball_pos, ball_speed = list(BALL_POS_0), list(BALL_SPEED_0)
    
    while True: # --- main loop ---
        ...
        # tempo (em segundos) que passou durante o ultimo quadro
        dt = clock.tick(FRAMERATE)/1000.
        
        # --- atualizacoes ---    
        ball_pos[0] += dt * ball_speed[0]       
        ...

Esse tipo de animação, conhecido como *time-based*, representa a forma mais realística de se fazer animações. Observe que você pode alterar o valor de *FRAMERATE* à vontade que a velocidade da bola ainda será 100 pixels/segundo (coordenada x). Quanto maior o FPS, mais suave será animação e mais recursos de CPU serão consumidos. Jogos comerciais funcionam perfeitamente bem com 30 FPS e, em teoria, não adianta aumentar para além de 60 FPS pois o olho humano não consegue distinguir períodos tão pequenos (por exemplo, a taxa de atualização da maioria dos monitores é 60 Hz).

A realidade é bem diferente, no entanto. Primeiro, estamos falando aqui de Python, uma linguagem interpretada. Querendo ou não o interpretador representa uma camada significativa a mais no quesito processamento. Segundo, há diversos jogos comerciais que disponibilizam uma opção "unlimited-framerate", onde o jogo passa agir em "tempo real", consumindo o máximo de CPU que o sistema operacional lhe permitir. Em Pygame isso equivaleria a fazer ``FRAMERATE = 0``, onde o FPS real pode atingir a casa dos milhares dependendo de diversos fatores (máquina em questão/complexidade dos gráficos) - isso porém não significa que o programa ocupará 100% da CPU, pois tanto Python quanto o sistema operacional não permitem.

Isso tudo significa que Python + Pygame não são adequados para jogos comerciais? Bom, primeiro, é preciso se lembrar que Pygame é um wrapper em torno da famosa biblioteca SDL (concebida em C/C++), a qual é destinada unicamente para gráficos 2D. Considerando boas técnicas de programação e um valor de ``FRAMERATE`` naturalmente elevado, Python + Pygame podem sim proporcionar um desempenho igualável ao de SDL em C/C++. Mas para jogos comerciais o ideal é utilizar OpenGL, que possibilita aceleração via hardware. É possível inclusive utilizar Pygame + PyOpenGL, onde abre-se possibilidade também para o 3D e várias técnicas de otimização.

Resumindo: estude e veja por si mesmo. Pygame é uma execelente porta de entrada para o mundo dos games. Muitos dos conceitos aprendidos com essa biblioteca vão se estender para as demais, considerando devidas adaptações de linguagens e frameworks. Fim do discurso, de volta ao Pong!

:mod:`3 - Organizando o código`
-------------------------------

O código desenvolvido até aqui tem meras 50 linhas, mas como você pode ter reparado, já possuimos uma série de variáveis e constantes globais, e várias "seções" de código que só tendem a crescer. Python permite, e por vezes encoraja, esse estilo de programação - e por isso mesmo é tida muitas vezes como linguagem de *scripting*. Entretanto, isto não é nem um pouco recomendável para o tipo de aplicação que vamos desenvolver: um jogo. Felizmente, Python é uma linguagem multi-paradigma, e logo, cabe a nós programadores fazer bom uso disso.

Por exemplo, a começar pela bola, o ideal seria agrupar seus atributos (cor, posição, velocidade) em uma estrutura única, como um dicionário por exemplo. No entanto, a solução mais elegante aqui é, evidentemente, utilizar orientação a objetos como segue::

    class Ball(object):
        def __init__(self, diameter, color, pos=(0, 0), speed=(0, 0)):
            ''' (int, pygame.Color, tuple of 2 numbers, tuple of 2 numbers)
            '''
            self.diameter, self.color = int(diameter), Color(color)
            self.pos, self.speed = list(pos), list(speed)

            self.surf = pygame.Surface(2*[self.diameter])
            self.surf.fill(self.color)
            self.surf.convert()
            
        def draw(self, dest_surf):
            ''' (pygame.Surface)
            '''
            dest_surf.blit(self.surf, self.pos)
        
        def run(self, dt):
            ''' (int) 
            '''
            self.pos[0] += dt * self.speed[0]
            self.pos[1] += dt * self.speed[1]
            
Utilizando classes, além de encapsular os atributos do objeto também disponibilizamos métodos apropriados para lidar com o objeto em si, de modo que o programa principal não precise se preocupar com esses detalhes particulares. 

Seja então o programa principal reescrito como::

    import pygame, sys
    from pygame.locals import *

    RESOLUTION, FRAMERATE = (400, 300), 0
    BACKGROUND_COLOR = Color('black')

    BALL_ATTRIBUTES = {
        'diameter': 20, 'color': 'white',
        'pos': (100, 100), 'speed': (100, 0)}
        
    class Ball(object):
        ...

    def main():
        # carrega todos os modulos de pygame (video, som, fontes, etc)
        pygame.init()
        
        # objeto pygame.Surface que contem o conteudo a ser exibido no monitor
        screen = pygame.display.set_mode(RESOLUTION)
        clock = pygame.time.Clock()
        
        ball = Ball(**BALL_ATTRIBUTES)
        
        while True: # --- main loop ---
            
            for event in pygame.event.get(): # --- tratamento de eventos ---
        
                if event.type == QUIT:
                    pygame.quit()       # desfaz pygame.init() e destroi a janela
                    sys.exit()          # encerra o interpretador python
                    
            # tempo (em segundos) que passou durante o ultimo quadro
            dt = clock.tick(FRAMERATE)/1000.
            
            # --- atualizacoes ---    
            ball.run(dt)
            
            # mantem a bola dentro da tela
            if ball.pos[0] > RESOLUTION[0]:
                ball.pos[0] = 0
            
            # --- desenhos ---
            screen.fill(BACKGROUND_COLOR)
            ball.draw(screen)
        
            pygame.display.update()     # atualiza a imagem no monitor
            
            
    if __name__ == '__main__':
        main()

O dicionário global ``BALL_ATTRIBUTES`` cumpre o papel das constantes globais que tínhamos antes. Apesar de não parecer muito necessário agora, quando o programa crescer e tiver vários objetos personalizáveis, essas configurações globais virão a calhar. Observe ainda que a classe ``Ball`` independe da existência dessa constante global. Na instanciação ``ball = Ball(...)``, os parâmetros foram "desempacotados" a partir de ``BALL_ATTRIBUTES`` com uso do operador ``**`` (desempacota dicionários).

O programa principal segue uma estrutura procedural, onde a função ``main()`` aqui cumpre função semelhante à sua análoga de C/C++. A priori, poderíamos manter o programa assim sem qualquer problemas, procedendo apenas criando os objetos auxiliares. Eu vou propor aqui a utilização de orientação a objetos também para o programa principal afim de melhor definir os trechos de código (main loop, tratamento de eventos, atualizações, etc).

Seja então o seguinte protótipo::

    import pygame, sys
    from pygame.locals import *

    class AppPong(object):
        
        RESOLUTION, FRAMERATE = (400, 300), 0
        background_color = Color('black')
        
        BALL_ATTRIBUTES = {
            'diameter': 20, 'color': 'white',
            'pos': (100, 100), 'speed': (100, 0)}    
        
        def __init__(self):
            self.screen = None      # pygame.Surface
            self.clock = None       # pygame.time.Clock    
            self.dt = 0     # periodo do ultimo quadro (em segundos)
            
        def _setup(self):
            pass
            
        def _update(self):
            pass
            
        def _draw(self):
            pass
            
        def execute(self):
            pass       
        
        def quit(self):
            pass
     
     
    class Ball(object):
        ...
            
    if __name__ == '__main__':
        AppPong().execute()

Primeiramente, veja que não há mais constantes globais, afinal, são todos atributos do programa em si. Porquê manter ``BALL_ATTRIBUTES``, se esses atributos serão copiados para a instância da classe ``Ball``? Simples: manter a classe ``Ball`` independente e ao mesmo tempo centralizar todas as configurações na classe ``AppPong``. De quebra, ``BALL_ATRIBUTES`` pode ser visto como um "backup" das configurações padrões, que podem ser restauradas a a qualquer momento.

Agora, quanto à ``__init__()``, porquê não criar a janela já na instanciação? Por uma questão de legibilidade do código. A idéia é que o método ``execute()`` faça juz ao seu nome: o programa só começa quando ele for chamado. Em Pygame, "programa começar" significa ``pygame.display.set_mode()`` ser chamada e o *main loop* iniciado em seguida. Nesse contexto, os métodos ``_setup()``, ``_update()`` e ``_draw()`` cumprirão o papel de separar as regiões de código conforme sua funcionalidade::

    ...
    class AppPong(object):
        ...
        def _setup(self):
            ''' 
            Prepara os objetos principais, podendo utilizar self.screen e
            self.clock. Executada antes do main loop.
            '''
            self.ball = Ball(**self.BALL_ATTRIBUTES)
        
        def _update(self):
            ''' 
            Atualiza o estado do jogo. Executada dentro do main loop, 
            antes de self.draw().
            '''
            self.ball.run(self.dt)
            
            # mantem a bola dentro da tela
            if self.ball.pos[0] > self.RESOLUTION[0]:  
                self.ball.pos[0] = 0  
        
        def _draw(self):
            ''' 
            Re-desenha os objetos na tela. Executada ao final do main loop.
            '''
            self.screen.fill(self.background_color)
            self.ball.draw(self.screen)
        ...

A grande beleza dessa abordagem é que o leitor do código pode se focar naquilo que ele quer saber: quais os objetos presentes no jogo? -> veja ``_setup()``; o que acontece na etapa das atualizações? -> veja ``_update()``; etc. 

Levando isso tudo em consideração, partimos para a implementação de ``execute()`` e ``quit()``::

    ...
    class AppPong(object):
        ...
        def execute(self):
            '''
            Cria/exibe a janela e dispara o main loop.
            '''
            pygame.init()
            self.screen = pygame.display.set_mode(self.RESOLUTION)
            self.clock = pygame.time.Clock()        
            self._setup()
            while True: # --- main loop ---            
                for event in pygame.event.get(): # --- tratamento de eventos ---        
                    if event.type == QUIT:
                        self.quit()
                        
                self.dt = self.clock.tick(self.FRAMERATE)/1000.
                self._update()
                self._draw()        
                pygame.display.update()     # atualiza a imagem no monitor        
        
        def quit(self):
            ''' 
            Encerra o programa adequadamente, destruindo a janela.
            '''
            pygame.quit()       # desfaz pygame.init() e destroi a janela
            sys.exit()          # encerra o interpretador python 
    ...
    
Nada de muito novo aqui, exceto que agora utilizamos ``self`` na frente das variáveis e métodos. Alungs podem argumentar que isso dificulta a escrita do código: talvez sim, por 4 caracteres a mais. No entanto, ganha-se muito em legibilidade: ao ver a palavra ``self`` na frente de um idenficador, fica imediatamente claro o seu escopo - ele é global para toda a classe. Além disso, pode-se mentalmente substituir ``self`` pelo nome da classe para entender melhor o que um atributo/variável ou método/função significam e porquê existem. Por fim, note como agora é bem mais fácil responder à pergunta "o que acontece no main loop?".

Para terminar esta seção, vamos adicionar a funcionalidade de encerrar o programa ao se apertar a tecla ESC. Além de ser útil para realização de testes consecutivos, também vai ilustrar aqui como pode ser feito o tratamento de eventos aproveitando-se dessa abordagem orientada a objetos::

    ...
    class AppPong(object):
        ...
        def onKeyDownEvent(self, key):
            if key == pygame.K_ESCAPE:
                self.quit()
        
        def execute(self):
            ...
            while True: # --- main loop ---            
                for event in pygame.event.get(): # --- tratamento de eventos ---        
                    if event.type == pygame.QUIT:
                        self.quit()
                        
                    if event.type == pygame.KEYDOWN:
                        self.onKeyDownEvent(event.key)
                ...
    ...
    
Porquê não fazer isso diretamente dentro do *main loop*, em ``execute()``? Você vai reparar que no teste ``if event.type == pygame.KEYDOWN`` já se tem 4 níveis de identação no código. Isto em geral é sinal de que você deve modularizar código. Além disso, a criação do método ``onKeyDownEvent()`` deixa imediatamente claro ao leitor do código a resposta da pergunta "o que acontece se eu apertar uma tecla?", de modo que ele não precisa explorar códigos "obscuros" dentro do *main loop*. 

Criamos aqui então a convenção de que todos métodos cujo nome tem a forma ``on##Event``, onde ``##`` representam o nome de algum evento pré-definido em Pygame, representam tratamento de eventos. Assim, futuramente serão criados também ``onKeyUpEvent()``, ``onMouseButtonDownEvent()``, etc..


:mod:`4 - Movendo a bola - sprites e vetores`
---------------------------------------------

Nesse ponto estamos aptos à avançar no pequeno projeto "Pong! - Pygame edition". Nossa bola já é capaz de se mover em qualquer direção mas precisamos que ela fique ricochetando no interior da tela. O ideal é que a classe ``Ball`` tenha um método ``bounce()``, onde ela mesma cuida dessa tarefa. Resta só definir como passar os valores limitantes. Pode ser interessante aqui fazer uso do objeto ``pygame.Rect``, em um novo atributo ``bounce_rect``, como segue::

    ...
    class Ball(object):
        def __init__(self, diameter, color, pos, speed, bounce_rect=None):
            ''' (int, pygame.Color, tuple of 2 numbers, tuple of 2 numbers, pygame.Rect)
            '''       
            ...
            self._width, self._height = self.surf.get_size()
            self.bounce_rect = bounce_rect
         
        ...
            
        def bounce(self):
            ''' (pygame.Rect)
            
            Mantem a bola dentro dos limites dados pelo retangulo self.bounce_rect.
            '''
            if not isinstance(self.bounce_rect, pygame.Rect):
                return
                
            W, H, BR = self._width, self._height, self.bounce_rect
            
            if self.pos[0] <= BR.left:          # limite esquerdo
                self.pos[0] = BR.left
                self.speed[0] *= -1
                
            if self.pos[0] + W >= BR.right:     # limite direito
                self.pos[0] = BR.right - W
                self.speed[0] *= -1
            
            if self.pos[1] <= BR.top:           # limite superior
                self.pos[1] = BR.top
                self.speed[1] *= -1
        
            if self.pos[1] + H >= BR.bottom:    # limite inferior
                self.pos[1] = BR.bottom - H
                self.speed[1] *= -1
        ...

As variáveis ``W``, ``H``, ``BR`` foram criadas apenas para facilitar a leitura do código. Em Python, atribuição de uma variável diretamente à outra não cria novos objetos, mesmo que esses sejam tipos imutáveis (inteiros, strings, etc) - ou seja, é apenas criada uma nova referência a um objeto já existente. 

O código do método ``bounce()`` é bem direto, sendo quatro testes - um para cada limite. No caso dos limites direito e inferior podemos contar com os atributos ``right`` e ``bottom`` do objeto ``pygame.Rect``, os quais já consideram as dimensões do retângulo. É importante que, ao ultrapassar um limite, a posição seja re-configurada para a posição limitante. Em seguida, é claro, a velocidade da coordenada em questão é invertida.

Queremos que o retângulo limitante (atributo ``bounce_rect``) compreenda toda a tela. Isso pode ser configurado em ``AppPong``::

    class AppPong(object):
        ...
        def _setup(self):
            ...
            self.BALL_ATTRIBUTES['bounce_rect'] = self.screen.get_rect()
            self.ball = Ball(**self.BALL_ATTRIBUTES)

O código de ``bounce()`` está bom, mas poderia ser um pouco melhor. Primeiramente, veja que coisas como ``pos[0]`` ou ``speed[1]`` não são nomes assim tão intuitivos para coordenadas de uma grandeza vetorial. Na prática é muito comum se criar uma classe ``Vector2D`` para guardar coordenadas e outras utilidades vetoriais. Aqui no caso,ela poderia ser usada tanto para o vetor posição como para o vetor velocidade

Em http://www.pygame.org/wiki/2DVectorClass você encontra classe bem completa para utilização de vetores 2D. Na intenção de manter esse programa auto-suficiente, vamos partir implementando apenas a funcionalidade básica e adicionando o que for preciso conforme surjam as necessidades. Seja então a pequena e simples classe::

    class Vector2D(object):
        def __init__(self, x=0, y=0):
            self.x, self.y = x, y
            
        def __len__(self):
            return 2
            
        def __getitem__(self, key):
            if key == 0:
                return self.x
            elif key == 1:
                return self.y
            else:
                raise IndexError("Invalid key '%s' for Vector2D!" % str(key))
            
Os métodos mágicos ``__len__()`` e ``__getitem__()`` fazem com que o objeto se comporte como um container (uma tupla, no caso). Assim, a instância pode ser passada diretamente para as funções de Pygame que esperam uma tupla/lista de 2 inteiros;

Agora, nesse ponto é importante olhar o "quadro geral" do programa. A classe ``Ball`` possui vários elementos necessários para fazer um objeto (superfície) se movimentar na tela. Esses mesmos elementos serão úteis para outros objetos que não a bola; nesse jogo em específico, falamos dos bastões dos jogadores. No jargão do game development existe o termo *Sprite*, utilizado para designar algo como "imagens que se movem na tela". De fato, Pygame oferece uma classe ``Sprite`` para auxiliar nas operações de manutenção dos itens móveis, mas ela não inclue os atributos e métodos já estabelecidos para nosso objeto ``Ball``.

A melhor coisa aqui é começarmos a implementar nossa própria classe ``Sprite`` (a qual futuramente poderá herdar de ``pygame.sprite.Sprite``), separando então as características gerais de objetos móveis das características particulares desses objetos. Que características gerais são essas? Oras, a posição, velocidade, métodos como ``run()``, ``draw()`` e ``bounce()``; ou seja, praticamente toda a classe ``Ball``.

Seja o seguinte protótipo::

    class Sprite(object):
        def __init__(self, pos=(0, 0), velocity=(0, 0), bounce_rect=None):
            ''' (tuple of 2 numbers, tuple of 2 numbers, pygame.Rect)
            '''
            self.pos, self.velocity = Vector2D(*pos), Vector2D(*velocity)
            self.bounce_rect = bounce_rect
            
            self.width, self.height = 0, 0
            self._surf = None
                        
            self.running = True        
            self.visible = True
            
        def setSurface(self, surf):
            pass
                    
        def draw(self, dest_surf):
            pass       
        
        def run(self, dt, bounce=False):
            pass      
                
        def bounce(self):
            pass

Ou seja, temos basicamente toda a classe ``Ball`` aqui, com a posição e velocidade já sendo vetores 2D. A única diferença aqui é que não criamos nenhuma superfície em particular. A idéia que essa classe não seja instanciada diretamente, e sim que os demais objetos do jogo (personagens, inimigos, projéteis, etc) herdem essa classe e então configurem sua superfície adequadamente usando o método ``setSurface()``, implementado como segue::

    class Sprite(object):
        ...
        def setSurface(self, surf):
            ''' (pygame.Suface)
            '''
            self._surf = surf
            self.width, self.height = self._surf.get_size()

OBS: Os atributos ``self.width`` e ``self.height`` devem ser entendidos como *somente para leitura*. Apenas o método ``setSurface`` deve alterá-los.

Veja que acrescentamos também a flag ``self.visible``, o que pode ser útil para que o sprite seja "ocultado" quando for necessário. Levando esses detalhes em consideração, o método ``draw()`` muda um pouco::

    class Sprite(object):
        ...
        def draw(self, dest_surf):
            ''' (pygame.Surface)
            
            Caso a flag self.visible esteja ativa e uma superficie tenha sido
            configurada com self.setSurface(), copia esta superficie para a
            superficie 'dest_surf', na posicao self.pos.
            '''
            if self.visible and self._surf:
                dest_surf.blit(self._surf, self.pos) 

Naturalmente, uma superfície deve ter sido configurada para que ela possa ser desenhada. Veja também que ``blit()`` espera uma tupla/lista de 2 inteiros para a posição, mas passamos ``self.pos``, um objeto ``Vector2D`` que se comporta como container. 

Por fim, levando em consideração que agora ``self.pos`` e ``self.velocity`` são objetos ``Vector2D``, temos os novos métodos ``run()`` e ``bounce()``::

    class Sprite(object):
        ...
        def run(self, dt, bounce=False):
            ''' (int, bool)
            
            Caso a flag self.running esteja ativa, movimenta a bola utilizando
            'dt', o período (em segundos) entre os quadros. Se 'bounce' for
            True, tambem invoca self.bounce().
            '''
            if not self.running: return
                
            self.pos.x += dt * self.velocity.x
            self.pos.y += dt * self.velocity.y            
            
            if bounce:
                self.bounce()        
                
        def bounce(self):
            '''        
            Mantem a bola dentro dos limites dados pelo retangulo 
            self.bounce_rect.
            '''
            if not isinstance(self.bounce_rect, pygame.Rect): return                
            BR = self.bounce_rect
            
            if self.pos.x <= BR.left:                   # limite esquerdo
                self.pos.x = BR.left
                self.velocity.x *= -1
                
            if self.pos.x + self.width >= BR.right:     # limite direito
                self.pos.x = BR.right - self.width
                self.velocity.x *= -1
            
            if self.pos.y <= BR.top:                    # limite superior
                self.pos.y = BR.top
                self.velocity.y *= -1
        
            if self.pos.y + self.height >= BR.bottom:   # limite inferior
                self.pos.y = BR.bottom - self.height
                self.velocity.y *= -1

Uma curiosidade: em inglês, *speed* indica velocidade escalar, ou seja, apenas o módulo da velocidade (o quão rápido está andando, não importa para que direção); já *velocity* indica velocidade vetorial, ou seja, considera-se todos os elementos da grandeza velocidade: seu módulo, direção e sentido.

Por fim, a nova classe ``Ball`` deve herdar ``Sprite``, onde apenas configuramos a superfície que queremos para a bola::

    class Ball(Sprite):
        def __init__(self, diameter, color, **kwargs):
            ''' (int, pygame.Color, Sprite.__init__(args))
            '''
            Sprite.__init__(self, **kwargs)
            self.diameter, self.color = int(diameter), pygame.Color(color)
            self.render()
                
        def render(self):
            '''
            (Re)cria a superficie do objeto com base nos atributos atuais.
            '''
            surf = pygame.Surface(2*[self.diameter])
            surf.fill(self.color)
            surf.convert()
            self.setSurface(surf)

Veja que apenas o diâmetro e a cor são atributos específicos do objeto ``Ball``; os demais foram capturados através do dicionário desempacotado ``**kwargs`` e diretamente encaminhados para ``__init__()`` da classe pai, ``Sprite``. Assim a instanciação pode ocorrer como ``ball = Ball(20, "blue", pos=(100, 200), velocity=(100, 100))``, onde a parte ``pos=(100, 200), velocity=(100, 100)`` é entendida como um "dicionário desempacotado". Na verdade, o uso de *keyword-arguments* é altamente encorajado para facilitar a legibilidade do código. Por exemplo, o ideal seria fazer, no programa principal, algo como::

    ball = Ball(diameter=20, color="blue", pos=(100, 100), 
                velocity=(100, 100))

Da forma como foi feito, ``Ball`` não precisa copiar parâmetro por parâmetro de ``Sprite``, e considerando que os argumentos de ``Sprite.__init__()`` são todos opcionais, a instanciação da classe ``Ball`` realmente só depende - em último caso - dela mesma.

Como você já deve ter observado, também introduzimos o método ``render()``, responsável por gerar a superfície com base nos atributos atualmente configurados. Isso significa que agora você pode alterar o diâmetro e em seguida chamar ``render()`` para ter uma nova imagem. A forma mais elegante de fazer isso seria usar as **properties** de Python para fazer com que, ao configurar uma cor ou diâmentro, ``render()`` seja automaticamente invocada. Por hora, vamos manter as coisas simples como estão.

Mesmo fazendo tudo isso, criando as classes ``Vector2D`` e ``Sprite``, assim como uma nova classe ``Ball``, o programa principal continua essencialmente o mesmo: temos uma bola ricochetando na tela. Isso é bom, mostra que nosso código está bem compartimentalizado. Podemos finalizar esta seção adicionando alguns comandos de teclado para controlar a velocidade da bola e seu estado de movimento, algo que apesar de não ser necessário para o programa final, pode vir a calhar quando na realização de testes.

Para fins de simplicidade vamos falar aqui de aumentar/diminuir o módulo das coordenadas x e y do vetor velocidade, e não o módulo do vetor em si (dado por raiz(x² + y²)). Isso significa que, por exemplo, se o vetor atual é (-100, 100), e desejamos decrementar 50 unidades das coordenadas, o vetor resultante será (-50, 50) - isto é, a coordenada x, que já era negativa, acabou na verdade sendo incrementada em 50, enquanto que a coordena y, positiva, foi de fato reduzida em 50.

Traduzindo a lógica discutida no parágrafo anterior para um novo método ``incr()`` em ``Vector2D``, temos::

    class Vector2D(object):
        ...
        def __repr__(self):
            return 'Vec2D(%s, %s)' % (self.x, self.y)
                
        def incr(self, dx=0, dy=0):
            if self.x < 0: dx *= -1
            self.x += dx
            if self.y < 0: dy *= -1
            self.y += dy
    
    
onde o método mágico ``__repr__()`` deve retornar uma string que representa o objeto. 

E assim, na classe principal podemos fazer::

    class AppPong(object):
        ...
        def _onKeyDownEvent(self, key):
            if key == pygame.K_ESCAPE:
                self.quit()
                
            elif key == pygame.K_SPACE:
                self.ball.running = not self.ball.running
                print('Ball %s running!' % \
                    {True: 'starts', False: 'stops'}[self.ball.running])                
                
            elif key == pygame.K_F7:  
                self.ball.velocity.incr(-50, -50)
                print('Decreased ball velocity:\n\t%s px/s' % self.ball.velocity)            
                
            elif key == pygame.K_F8:                    
                self.ball.velocity.incr(50, 50)
                print('Increased ball velocity:\n\t%s px/s' % self.ball.velocity)
        ...
        
Agora você pode apertar ESPAÇO para ativar/desativar o movimento da bola, e as teclas F7/F8 para, respectivamente, diminuir ou aumentar a velocidade em passos de 50 unidades. 

Você vai reparar que, quando a velocidade for reduzida para zero, digamos indo de (-50, 50) para (0, 0), ao incrementar novamente ela passa a ser (50, 50) - ou seja, as direções anteriores foram completamente descartadas! Isso é um bug que nos faz repensar a classe ``Vector2D``: talvez o ideal seja, para cada coordenada, manter o módulo em um atributo e o sentido em outro. É importante, porém, que a nova implementação de ``Vector2D`` não complique o seu uso nos demais objetos.

Começamos então definindo os atributos privados ``_x`` e ``_y`` para guardar as magnitudes (não podem ser negativos, portanto), e ``_i`` e ``_j`` para guardar os respectivos sinais/sentidos (que pode ser 1 ou -1)::

    class Vector2D(object):
        def __init__(self, x=0, y=0):
            self._x, self._y, self._i, self._j = 0, 0, 1, 1
            self.x, self.y = x, y
        ...            
            
Agora, configurar as coordenadas implica em separar magnitude de sinal. Em linguagens como C++ ou Java isso levaria na criação de *getters* e *setters*, tais como ``setX()`` e ``getY()``, o que não é desejado aqui; afinal, fazer ``self.x`` para configurar a coordenada x ainda é mais interessante. Python permite atingir isso utilizando **properties**, como segue::

    ...
    import math
    ...
    class Vector2D(object):
        ...
        @property
        def x(self):
            return self._x * self._i
            
        @x.setter
        def x(self, value):
            self._x, self.i = abs(value), value
                
        @property
        def y(self):
            return self._y * self._j
            
        @y.setter
        def y(self, value):
            self._y, self.j = abs(value), value
        
        @property
        def i(self):
            return self._i
            
        @i.setter
        def i(self, value):
            self._i = int(math.copysign(1, value))
            
        @property
        def j(self):
            return self._j
            
        @j.setter
        def j(self, value):
            self._j = int(math.copysign(1, value))

Através da classe *built-in* ``property`` é possível associar métodos especiais para atuarem como *getters* e *setters* de atributos públicos. Aqui, ``x``, ``y``, ``i`` e ``j`` são atributos tais que podem ser manipulados livremente enquanto se mantém a estrutura interna de ``Vector2D``. Utilizamos aqui a notação de *decorators*, através da qual se usa o operador ``@`` para "decorar" uma função/método/classe de modo a acrescentar-lhe alguma funcionalidade/significado.

Ao fazer ``@property`` sobre um método, transformamos este num *getter*. Os *getters* das coordenadas retornam o que se esperaria: tanto módulo quanto o sentido; já os *getters* ``i`` e ``j`` simplesmente retornam os valores de ``_i`` e ``_j`` para conveniência. Uma vez tendo criada uma *property*, pode-se configurar seu *setter* e/ou *deletter* também utilizando decorators.

Começemos pelos *setters* de ``i`` e ``j``. Esses valores representam apenas o sentido da coordenada, podendo ser ou 1, ou -1. Independente do valor que o usuário passar, só precisamos extrair o sinal juntamente com o número 1 - daí a função ``math.copysign(x, y)``, que retorna x com o sinal de y. Além de permitir a inversão do sentido fazendo-se algo como ``velocity.i *= -1``, essa abordagem facilita a criação dos *setters* para os atributos ``x`` e ``y``, onde basicamente se guarda o valor absoluto em ``_x`` e ``_y``, respectivamente, e extrai-se o sinal de ``value``, guardando-o em ``_i`` e ``_j``, respectivamente.
        
Tendo essa nova estrutura pronta, podemos então finalmente alterar o método ``incr()`` para atingir o objetivo almejado: corrigir o bug de incrementar a velocidade. A idéia então é incrementar/decrementar apenas a magnitude das coordenadas, motivo pelo qual renomeamos o método para ``incrMagXY()``::

    class Vector2D(object):
        ...
        def incrMagXY(self, dx=0, dy=0):
            ''' (number, number)
            
            Incrementa a magnitude das coordenadas. No caso de incrementos
            negativos, o limite inferior é, naturalmente, zero.
            '''
            self._x += dx
            if self._x < 0:
                self._x = 0
                
            self._y += dy
            if self._y < 0:
                self._y = 0
                
As alterações no resto do programa incluem a substituição de ``incr`` por ``incrMagXY`` em ``AppPong._onKeyDownEvent()`` e a reescrita da inversão do sentido da velocidade em ``Sprite.bounce()`` como segue::

    class Sprite(object):
        ...
        def bounce(self):
            ...
            if self.pos.x <= BR.left:                   # limite esquerdo
                self.pos.x = BR.left
                self.velocity.i *= -1
                
            if self.pos.x + self.width >= BR.right:     # limite direito
                self.pos.x = BR.right - self.width
                self.velocity.i *= -1
            
            if self.pos.y <= BR.top:                    # limite superior
                self.pos.y = BR.top
                self.velocity.j *= -1
        
            if self.pos.y + self.height >= BR.bottom:   # limite inferior
                self.pos.y = BR.bottom - self.height
                self.velocity.j *= -1
    
    
:mod:`5 - Bastões dos jogadores`
--------------------------------

Já temos uma bola (ok, é quadrada, mas foco na funcionalidade!) ricocheteando alegremente dentro da tela. Você inclusive pode aumentar/diminuir a velocidade dela como quiser. Nosso próximo passo será implementar os bastões dos jogadores, que serão simplesmente dois retângulos em lados opostos (esquerda/direita) da tela. Eles serão controlados com o teclado, digamos W/S para subir/descer o bastão do player 1 (esquerda), e UP/DOWN para subir/discer o bastão do player 2 (direita). Ok, mãos à obra!

Considerando que já possuímos a conveniente classe ``Sprite``, a classe dos bastões pode ser implementada como segue::

    class Paddle(Sprite):
        def __init__(self, size, color, **kwargs):
            ''' (tuple of 2 int, pygame.Color, Sprite args)
            '''
            Sprite.__init__(self, **kwargs)
            self.size, self.color = [int(x) for x in size], Color(color)
            self.render()
        
        def render(self):
            surf = pygame.Surface(self.size)
            surf.fill(self.color)
            surf.convert()
            self.setSurface(surf)

A única diferença com relação à classe da ``Ball`` é a forma como o tamanho da superfície foi passado. A *comprehension* ``[int(x) for x in size]`` apenas garante que o tamanho seja uma lista de inteiros (requisito para ``pygame.Surface``). 

Na classe principal, podemos facilmente criar dois objetos ``Paddle``, um para cada player, como segue::

    class AppPong(object):
        ...
        PADDLE_ATTRIBUTES = {
            'size': (15, 60), 'color': 'purple'}
        ...       
        def _setup(self):
            ...  
            self.p1_paddle = Paddle(**self.PADDLE_ATTRIBUTES)        
            
            p2_paddle_attr = dict(self.PADDLE_ATTRIBUTES)
            p2_paddle_attr['pos'] = \
                (self.RESOLUTION[0] - p2_paddle_attr['size'][0], 0)
            self.p2_paddle = Paddle(**p2_paddle_attr)            
        
        def _draw(self):
            ...
            self.screen.fill(self.background_color)
            self.p1_paddle.draw(self.screen)
            self.p2_paddle.draw(self.screen)
            self.ball.draw(self.screen)

O dicionário ``PADDLE_ATTRIBUTES`` contém configurações comuns a ambos os bastões. Como não há configurado nele um atributo ``'pos'``, no bastão do player 1 será assumido o valor default ``(0, 0)``, o que está OK. Já para o player 2 foi necessário duplicar ``PADDLE_ATTRIBUTES``, e na sua cópia, definir o atributo ``'pos'`` adequado: a coordenada x é quase o extremo direito da tela. Repare, por fim, que preferimos desenhar os bastões antes da bola.

Muito bem, tendo os bastões desenhados podemos proceder fazendo eles se moverem. O efeito desejado é que, enquanto uma certa tecla fique pressionada, o bastão se mova continuamente em uma dada direção. Os bastões são *sprites*, e já temos o aparato para fazer *sprites* se moverem. Nesse caso basta usarmos apenas a coordenada y (e direção j) do vetor velocidade.

Vamos começar de cima para baixo, definindo o mapeamento de teclas e os eventos de teclado na classe principal::

    class AppPong(object):
        ...
        PADDLE_ATTRIBUTES = {
            'size': (15, 60), 'color': 'purple',
            'velocity': (0, 400)}
            
        P1_KEYS = {pygame.K_w: 'up', pygame.K_s: 'down'}
        P2_KEYS = {pygame.K_UP: 'up', pygame.K_DOWN: 'down'}        
        ...
        def _update(self):
            ...
            self.ball.run(self.dt, bounce=True)
            self.p1_paddle.run(self.dt)
            self.p2_paddle.run(self.dt)
        
        def _onKeyDownEvent(self, key):
            if key == K_SPACE:
                ...
            elif key in self.P1_KEYS:
                self.p1_paddle.setMoveDirection(self.P1_KEYS[key])
                
            elif key in self.P2_KEYS:
                self.p2_paddle.setMoveDirection(self.P2_KEYS[key])  

        def _onKeyUpEvent(self, key):
            if key in self.P1_KEYS:
                self.p1_paddle.setMoveDirection(None)
                
            elif key in self.P2_KEYS:
                self.p2_paddle.setMoveDirection(None)
                
        def execute(self):
            ...
            while True: # --- main loop ---    
                for event in pygame.event.get(): # --- tratamento de eventos ---        
                    if event.type == pygame.QUIT:
                        ...
                    elif event.type == pygame.KEYUP:
                        self._onKeyUpEvent(event.key)
                ...
        ...
        
Nas configurações globais inauguramos o atributo ``'velocity'`` para os bastões (apenas com velocidade y), e também definimos o mapeamento de teclas para cada player nos dicionários ``P1_KEYS`` e ``P2_KEYS``. Optamos pela sintaxe ``tecla: movimento`` para facilitar a verificação nos eventos de teclado. Por exemplo, em ``_onKeyDownEvent()``, se uma tecla cadastrada em qualquer um dos dicionários foi apertada, chama-se ``setMoveDirection()`` no bastão correspondente, passando como argumento a direção correspondente à tecla em questão.

Inauguramos também o método ``_onKeyUpEvent()``, onde naturalmente testamos se ocorreu o levantamento de uma tecla. A verificação procede da mesma maneira que em ``_onKeyDownEvent()``, exceto que agora ``setMoveDirection()`` recebe ``None``, do que se interpreta que o bastão deve parar de se mover.

Isso tudo nos leva à implementação de ``setMoveDirection()`` na classe ``Paddle``::

    class Paddle(Sprite):
        def __init__(...):
            ...
            self.running = False        
        ...
        def setMoveDirection(self, direction):
            ''' ('up'/'down'/None)
            
            Configura a direção atual de movimento. Se for 'up' ou 'down', 
            ativa self.running; caso contrário, desativa self.running.
            '''
            if direction == 'up':
                self.velocity.j = -1
            elif direction == 'down':
                self.velocity.j = 1
            else:
                self.running = False
                return
            self.running = True

Nenhum mistério aqui, certo? Ela faz duas coisas: configura adequadamente a componente ``j`` do vetor velocidade, e ativa/desativa o movimento configurando a flag ``running``.

Caso você realize alguns testes, verá que há um problema com essa abordagem de controle utilizando o teclado: mudanças repentinas na direção pelo rápido acionamento de - possívelmente múltiplas - teclas podem não surtir efeito nos bastões. Isso não é bom, pois em geral queremos uma responsividade mais imediata o possível para os jogadores; de fato, muitos concordam que o componente mais importante de um jogo é sua jogabilidade.

Há uma outra maneira de testar pressionamento de teclas em Pygame: verificando o estado da tecla diretamente. A função ``pygame.key.get_pressed()`` retorna uma lista de booleanos contendo o estado de todas as teclas, onde cada índice nessa lista corresponde a um código de tecla. Assim, se ``key_state`` for o nome dessa lista, pode-se testar se a tecla W está pressionada fazendo ``key_state[pygame.K_w]``, expressão que retorna ``True`` se a tecla estiver apertada, e ``False`` caso contrário.

Apenas para ter uma consistência com a notação já estabelecida no programa, vamos definir o método ``_onKeysPressedEvent()``, apesar de que não se trata realmente de um "evento" por baixo dos panos.

Seja::

    class AppPong(object):
        ...
        def _onKeysPressedEvent(self, key_state):
            ''' (list of bool)
            '''
            for k, direction in self.P1_KEYS.items():
                if key_state[k]:
                    self.p1_paddle.setMoveDirection(direction)
            
            for k, direction in self.P2_KEYS.items():
                if key_state[k]:
                    self.p2_paddle.setMoveDirection(direction)
            
        def execute(self):
            ...
            while True: # --- main loop ---    
                self._onKeysPressedEvent(pygame.key.get_pressed()) 
                
                for event in pygame.event.get(): # --- tratamento de eventos ---
                    ...
                ...
                
A cada iteração do *main loop* será testado o estado das teclas presentes nos dicionários ``P1_KEYS`` e ``P2_KEYS``. Assim, podemos remover o código adicionado em ``_onKeyDownEvent()``, porém mantendo código de ``_onKeyUpEvent()`` (necessário para desativar o movimento dos bastões).

Muito bem, os bastões estão movimentáveis - falta apenas restringir o seu movimento para não ultrapassar os limites superior/inferior da tela. Felizmente o método ``Sprite.bounce()`` já proporciona esse efeito. Claro, as velocidades serão invertidas ao rebater nos limites superior/inferior, mas isso não é problema para os bastões, pois a direção é comandada pelo pressionamento de teclas. 

Basta então configurar o retângulo limitante tal como foi feito para a bola::

    class AppPong(object):
        ...
        def _setup(self):
            ...        
            self.PADDLE_ATTRIBUTES['bounce_rect'] = self.screen.get_rect()
                    
            self.p1_paddle = Paddle(**self.PADDLE_ATTRIBUTES)
            ...
        
        def _update(self):
            ...
            self.ball.run(self.dt, bounce=True)         # movimento livre
            self.p1_paddle.run(self.dt, bounce=True)    # controle pelo teclado
            self.p2_paddle.run(self.dt, bounce=True)    # controle pelo teclado
            

:mod:`6 - Detalhes de cenário`
------------------------------

Está na hora de adicionarmos alguns elementos de cenário, como as marcas do campo e os contadores de pontuação. Em termos de marcas do campo, queremos disponibilizar:

* Duas bordas, uma superior e outra inferior, para sinalizar a região em que a bola está contida;
* Um sinal de divisão do campo, dado por uma linha "pontilhada" no centro do campo.

Ou seja, na verdade queremos um "plano de fundo" diferente para o nosso jogo. Na verdade, atualmente não se tem um plano de fundo, por assim dizer - apenas pintamos o fundo com ``BACKGROUND_COLOR`` a cada iteração do *main loop*. O que faremos então é disponibilizar uma superfície, de tamanho igual à da tela, contendo o plano de fundo com os desenhos desejados. Agora, essa superfície será blitada e ``screen`` no lugar de ``screen.fill()``.

Essa imagem de plano de fundo poderia ser facilmente desenhada em um editor de imagem, com tamanho 400 x 300 (ou qual seja a resolução utilizada), e então carregada em uma superfície com ``pygame.image.load()`` para ser blitada diretamente em ``AppPong._draw()``. Entretanto, considerando a simplicidade da imagem, o que faremos é desenhar ela via código, como segue::

    class FieldBackground(object):    
        BLIT_POS = (0, 0)
        
        def __init__(self, dest_surf, **kwargs):
            ''' (pygame.Surface, kwargs)
            
            kwargs:
                - background_color: pygame.Color.
                - mark_color: pygame.Color.
                - border_width: int.
                - divisor_width: int.
                - divisor_dash_lenght: int.
                - divisor_dash_spacing: int.
            '''
            self.dest_surf = dest_surf

            self.surf = pygame.Surface(dest_surf.get_size())
            self.surf.fill(pygame.Color(kwargs.get('background_color', 'black')))

            W, H = dest_surf.get_size()
            mark_color = pygame.Color(kwargs.get('mark_color', 'green'))
            border_width = kwargs.get('border_width', 10)
            
            # borda superior    
            border_rect = pygame.Rect(0, 0, W, border_width)
            pygame.draw.rect(self.surf, mark_color, border_rect)        
            # borda inferior
            border_rect.top = H - border_width
            pygame.draw.rect(self.surf, mark_color, border_rect) 
            
            # linha divisoria (pontilhada)
            divisor_width = kwargs.get('divisor_width', 5)
            dash_lenght = kwargs.get('divisor_dash_lenght', 20)
            dash_spacing = kwargs.get('divisor_dash_spacing', 10)
            dash_rect = pygame.Rect(W//2 - divisor_width//2, border_width,
                                    divisor_width, dash_lenght)
                            
            while dash_rect.top < H:
                pygame.draw.rect(self.surf, mark_color, dash_rect)
                dash_rect.top += dash_lenght + dash_spacing
                            
            self.surf.convert()
                    
        def draw(self):
            self.dest_surf.blit(self.surf, self.BLIT_POS)
            
Essa classe contém basicamente 3 coisas: no atributo ``surf``, a superfície que contém o desenho do plano de fundo; no atributo ``dest_surf`` a superfície de destino para a blitagem, e por fim, o método ``draw()`` para uso na classe principal. Preferimos capturar aqui a superfície de destino como atributo justamente porquê o desenho de ``self.surf`` depende das dimensões desta superfície.

Falando então do código que faz os desenhos, todos os parâmetros necessários para tal foram capturados a partir do dicionário ``kwargs`` - afinal, já que estamos desenhando via código podemos muito bem deixar o objeto aberto à personalização. As bordas superior e inferior são meros retângulos, desenhados com ``pygame.draw.rect()`` em ``self.surf``. Já no caso da linha divisória, obtemos o efeito de "pontilhado" desenhando uma série de retângulos, dados por ``dash_rect``. A cada retângulo desenhado incrementa-se a altura do próximo retângulo de acordo com ``dash_spacing``.

Na classe principal, além de disponibilizar uma instância de ``FieldBackground``, devemos reconfigurar o retângulo limitante tanto para a bola quanto para os bastões, de modo a "reconhecer" as bordas do campo::

    class AppPong(object):
    
        RESOLUTION, FRAMERATE = (400, 300), 0
        
        BACKGROUND_FIELD_ATTRIBUTES = {
            'background_color': 'black', 'mark_color': 'green', 
            'border_width': 10, 'divisor_width': 5, 'divisor_dash_lenght': 20,
            'divisor_dash_spacing': 10}
        
        BALL_ATTRIBUTES = ...
        ...   
        def _setup(self):
            ''' 
            Prepara os objetos principais, podendo utilizar self.screen e
            self.clock. Executada antes do main loop.
            '''        
            FA = self.BACKGROUND_FIELD_ATTRIBUTES
            BA, PA = self.BALL_ATTRIBUTES, self.PADDLE_ATTRIBUTES
            
            limit_rect = pygame.Rect(0, FA['border_width'], self.RESOLUTION[0],
                self.RESOLUTION[1] - 2*FA['border_width'])

            # plano de fundo do campo        
            self.field_background = FieldBackground(self.screen, **FA)
            
            # bola que se movimenta livremente
            BA['bounce_rect'] = limit_rect
            self.ball = Ball(**BA)
            
            # bastoes para os jogadores (movimento via teclado)
            PA['bounce_rect'] = limit_rect
            PA['pos'] = [0, FA['border_width']]                
            self.p1_paddle = Paddle(**PA)
            PA['pos'][0] = self.RESOLUTION[0] - PA['size'][0]
            self.p2_paddle = Paddle(**PA)
            
        ...        
        def _draw(self):
            ''' 
            Re-desenha os objetos na tela. Executada ao final do main loop.
            '''
            self.field_background.draw()
            self.p1_paddle.draw(self.screen)
            ...
            
Manter nomes explícitos para os dicionários de configurações globais, tal como ``BACKGROUND_FIELD_ATTRIBUTES`` facilita a legibilidade do código no local em que foram criados. Mas em ``_setup()`` necessitamos fazer vários acessos aos dicionários, de modo que torna-se mais conveniente renomeá-los aqui para ``FA`` (abrevia *field attributes*), ``BA`` (abrevia *ball atributes*) e ``PA`` (abrevia *paddle attributes*).

O novo retângulo limitante foi definido em ``limit_rect``, considerando a largura das bordas do campo (``FA['border_width']``). Por fim, foi necessário reestruturar a instanciação dos bastões para considearar posições iniciais válidas - i.e., que também incluam ``FA['border_width']``. Inicialmente definmos ``PA['pos']`` com a coordenada x = 0 (player 1) e coordenada y no topo do tela permitida. Para o player 2 modificamos apenas a coordenada x de ``PA['pos']`` de modo que o bastão fique no extremo esquerdo da tela.

O próximo passo será definir os marcadores de pontuação, e para isso, precisamos criar um objeto que mostre um texto na tela. Naturalmente, estaremos usando recursos do módulo ``pygame.font``, o qual já foi inicializado com ``pygame.init()`` e portanto já é capaz de detectar fontes do sistema através do objeto ``pygame.font.SysFont``. Uma vez carregada a fonte, pode-se usar o método ``render()`` para transformar um texto em uma superfície.

Antes de prosseguir, no entanto, preciamos decidir como configurar a posição onde o texto vai aparecer, considerando que deseja-se um texto centralizado em alguma região: o score do player 1 deve ficar na metade esquerda da tela, e o score do player 2, na metade direita. De novo, é interessante aqui fazer uso do objeto ``pygame.Rect``, onde ao invés de passar a posição como parâmetro, passamos um retângulo que define a região onde o texto deverá estar contido. A centralização do texto nesse retângulo é algo bem simples de ser alcançado.

Acompanhe a classe para os marcadores de pontuação::

    class ScoreLabel(object):
        def __init__(self, text_rect, text_color, font_family='Verdana', 
                     font_size=20, bold=True):
            ''' (str, int, pygame.Color, bool)
            '''
            self.text_rect = pygame.Rect(text_rect)
            self._blit_pos = self.text_rect.topleft
            self.text_color = pygame.Color(text_color)
            self.font = pygame.font.SysFont(font_family, font_size, bold)
            
            self.surf = None
            self._points = 0
            self.render()
            
        @property
        def points(self):
            return self._points
        
        @points.setter
        def points(self, value):
            self._points = value
            self.render()
            
        def render(self):
            self.surf = self.font.render(str(self._points), 1, self.text_color)
            W, H = self.surf.get_size()
            RC = self.text_rect
            self._blit_pos = (RC.x + (RC.w// 2 - W//2),
                              RC.y + (RC.h// 2 - H//2))
            
        def draw(self, dest_surf):
            dest_surf.blit(self.surf, self._blit_pos)

O parâmetro ``text_rect`` define o retângulo que deverá conter o texto; observe que, apesar de já esperamos um argumento de tipo ``pygame.Rect``, efetuamos mesmo assim uma cópia do mesmo de modo a evitar "passagem por referência". A posição de blitagem, ``_blit_pos``, deverá ser re-calculada sempre que uma nova superfície de texto for renderizada. Também disponibilizamos a *property* ``points``, onde ao se alterar a pontuação o método ``render()`` é chamado automaticamente.

Falando então de ``ScoreLabel.render()``, utiliza-se ``font.render()`` com os parâmetros: texto a ser renderizado; flag de antialias (1 para sim); e a cor do texto. Esse método retorna uma superfície alpha contendo todos os pixels transparentes, exceto pelo texto. Uma vez criada a superfície do texto, precisamos recalcular a posição de blitagem levando em consideração as possíveis novas dimensões de ``self.surf``. Novamente aqui criamos nomes curtos para alguns valores de modo a facilitar a leitura. Os fatores de centralização horizontal ``(RC.w// 2 - W//2)`` e vertical ``(RC.h// 2 - H//2)`` já devem ser conhecidos do leitor nesse ponto.

O uso desta classe no programa principal se faz como segue::

    class AppPong(object):
        ...
        BACKGROUND_FIELD_ATTRIBUTES = ...
            
        SCORE_LABEL_ATTRIBUTES = {
            'text_color': 'green', 'font_family': 'Verdana', 'font_size': 30,
            'bold': True}
        
        BALL_ATTRIBUTES = ...        
        ...
        def _setup(self):
            ...
            BA, PA = self.BALL_ATTRIBUTES, self.PADDLE_ATTRIBUTES
            SLA = self.SCORE_LABEL_ATTRIBUTES
            
            ...
            self.p2_paddle = Paddle(**PA2)
            
            # marcadores de pontuacao
            text_rect = pygame.Rect(0, FA['border_width'], self.RESOLUTION[0]//2, 
                                    3*SLA['font_size'])
            SLA1, SLA2 = dict(SLA) , dict(SLA)
            SLA1['text_rect'] = text_rect
            SLA2['text_rect'] = pygame.Rect(text_rect)
            SLA2['text_rect'].x = self.RESOLUTION[0]//2
            
            self.p1_score_label = ScoreLabel(**SLA1)
            self.p2_score_label = ScoreLabel(**SLA2)
        
        def _draw(self):
            ...
            self.field_background.draw()
            self.p1_score_label.draw(self.screen)
            self.p2_score_label.draw(self.screen)
            self.p1_paddle.draw(self.screen)
            self.p2_paddle.draw(self.screen)        
            self.ball.draw(self.screen)
                        
        def _onKeyDownEvent(self, key):
            if key == pygame.K_ESCAPE:
                ...           
                    
            elif key == pygame.K_o:
                self.p1_score_label.points += 1
                
            elif key == pygame.K_p:
                self.p2_score_label.points += 1                
                
Os retângulos que contém os textos dos *score labels* são idênticos para o player 1 e o player 2, exceto pela coordenada x, que no caso do player 2, é exatamente a metade da tela. A largura desses retângulos é a metade da tela, e a altura, 3 vezes o tamanho da fonte configurada no dicionário ``SLA``. Isso é suficiente para que toda a superfície caiba dentro do retângulo e ainda sobre algum espaço entre o texto e a borda superior. 

Por fim, no código acima também adicionamos a função de aumentar o escore dos players usando as teclas O e P para testes.


:mod:`7 - Rebatendo a bola`
---------------------------

Muito bem, está na hora de finalmente implementarmos a funcionalidade principal desse jogo: rebater a bola com os bastões. Uma maneira simples de alcançar esse efeito seria estabelermos uma linha imaginária imediatamente à frente dos bastões: caso a bola ultrapasse essa linha E o bastão esteja nessa região, entende-se uma colisão e a bola é refletida. Outra maneira, mais elegante e apropriada, é utilizar o mecanismo de colisões entre retângulos oferecido por Pygame.

De uma forma ou de outra é conveniente mantermos a informação sobre o retângulo dos *sprites*, o que nos leva ao seguinte incremento na classe ``Sprite``::

    class Sprite(object):
        def __init__(self, pos=(0, 0), velocity=(0, 0), bounce_rect=None):
            ...
            self._surf, self.rect = None, pygame.Rect(pos, (0, 0))
            ...
            
        def setSurface(self, surf):
            ...
            self.width, self.height = surf.get_size()
            self.rect.size = surf.get_size()
        
        def run(self, dt, bounce=False):
            ...
            self.pos.x += dt * self.velocity.x
            self.pos.y += dt * self.velocity.y
            
            self.rect.topleft = round(self.pos.x), round(self.pos.y)
            ...
            
É importante observar que os objetos ``pygame.Rect`` não aceitam valores de ponto flutuante, por isso usamos ``round()`` para arredondar a posição atual. Por esse mesmo motivo não é possível utilizarmos apenas os retângulos para manter os coordenadas de posição dos *sprites*, já que na animação *time-based* aqui implementada, os incrementos de posição podem facilmente atingir valores menores que 0.5, de modo que seria arredondado para 0 e perderia-se a informação para que o incremento seguinte funcione como deveria. Isso sem falar que o uso de ``Vector2D`` tem suas vantagens, como já observamos.

Apesar de ser "irrelevante" para o movimento dos *sprites* da forma como implementamos aqui, os objetos ``pygame.Rect`` oferecem métodos de tratamento de colisão que podem facilitar um pouco a nossa vida. Dois retângulos estão em colisão quando possuem pontos em comum; isso poderia ser facilmente implementado usando-se os valores de ``self.pos`` e as dimensões do *sprite*, mas podemos simplesmente fazer::

    class Sprite(object):
        ...
        def collidesWith(self, sprite):
            ''' (Sprite)
            '''
            return self.rect.colliderect(sprite.rect)

Agora nossos sprites são capazes de reconhecer colisões entre si. Mantenha em mente que foi utilizada aqui colisão retangular, a forma mais básica e mais limitada de se detectar colisões. Futuramente esse método ``collideWith()`` poderá ser reformulado para oferecer também colisões circulares e/ou colisões perfeitas (pixel a pixel).

Para criar o efeito da bola sendo rebatida pelos bastões, podemos testar a colisão da bola com os bastões, ou vice-versa. Vamos optar aqui por implementar a detecção e o tratamento da colisão com os bastões dentro da classe ``Ball``::

    class Ball(Sprite):
        ...        
        def collidesWithPaddle(self, paddle):
            ''' (Paddle)
            '''
            if self.collidesWith(paddle):   
            
                print('Player %s hit the ball!' % str(paddle.player))
                self.velocity.i *= -1            
                
                if paddle.player == 1:
                    self.pos.x = paddle.rect.right
                elif paddle.player == 2:
                    self.pos.x = paddle.rect.x - self.width
                    
                if paddle.running:
                    self.velocity.j = paddle.velocity.j
                    
Esse código assume a existência de um atributo ``Paddle.player``, número inteiro que identifica de qual jogador é o bastão, podendo ser 1 ou 2. Assim, o código acima fica bem fácil de se ler: caso ocorra colisão com um bastão, inverte-se a velocidade x da bola e ajusta-se a posição da mesma para "fora" do respectivo bastão. Caso o bastão esteja se movendo, a direção ``j`` da bola acompanhará a direção ``j`` do bastão, o que colabora com a impressão de que o bastão bateu na bola, alterando sua direção.

Considerando as alterações em ``Paddle`` e na classe principal, temos::

    class AppPong(object):
        ...
        def _setup(self):
            ...
            # bastoes para os jogadores (movimento via teclado)
            PA['bounce_rect'] = limit_rect      # comum a ambos
            PA1, PA2 = dict(PA), dict(PA)
            PA1['pos'], PA1['player'] = (0, FA['border_width']), 1
            PA2['pos'] = (self.RESOLUTION[0] - PA['size'][0], FA['border_width'])
            PA2['player'] = 2
            ...
                
        def _update(self):
            ...
            self.ball.collidesWithPaddle(self.p1_paddle)
            self.ball.collidesWithPaddle(self.p2_paddle)
            
        ...

    class Paddle(Sprite):
        def __init__(self, player, size, color, **kwargs):
            ''' (int, tuple of 2 int, pygame.Color, Sprite args)
            '''
            ...
            self.player = player
            ...

Temos assim implentado um sistema básico de colisão bola-bastão. Não é o ápice da perfeição, mas já permite alguma jogabilidade, de modo que podemos nos concentrar em finalizar a mecânica básica do jogo. Futuramente poderemos então refatorar o código, adicionando um pouquinho de física e maior realismo nas jogadas.

Muito bem, caso a bola não seja rebatida, precisamos criar algum efeito de *round over* e incrementar a pontuação do adversário. Em outras palavras, precisamos ser capazes de dizer se a bola colidiu com o limite esquerdo ou direito da tela. Alcançamos isso fazendo ``Sprite.bounce()`` retornar um valor que identifica qual(is) limites foram atingidos::

    class Sprite(object):
        ...
        def run(self, dt, bounce=False):
            ...
            if bounce:
                return self.bounce()        
        
        def bounce(self):
            ''' () -> str
            
            Mantem a bola dentro dos limites dados pelo retangulo 
            self.limit_rect. Retorna uma string contendo os caracteres 'L', 'R',
            'T', 'B' que identificam os limites atingidos.
            '''
            ...
            BR, bounced = self.bounce_rect, ''
            
            if self.pos.x <= BR.left:                   # limite esquerdo
                ...
                bounced += 'L'
                
            if self.pos.x + self.width >= BR.right:     # limite direito
                ...
                bounced += 'R'
            
            if self.pos.y <= BR.top:                    # limite superior
                ...
                bounced += 'T'
        
            if self.pos.y + self.height >= BR.bottom:   # limite inferior
                ...
                bounced += 'B'
            
            return bounced
            
Feito isso, na classe principal podemos fazer::

    class AppPong(object):
        ...
        def _setup(self):
            ...            
            self.paused = False
                
        def _update(self):
            ''' 
            Atualiza o estado do jogo. Executada dentro do main loop, 
            antes de self.draw().
            '''
            if self.paused: return
            
            self.p1_paddle.run(self.dt, bounce=True)
            self.p2_paddle.run(self.dt, bounce=True)        
            
            ball_bounced = self.ball.run(self.dt, bounce=True)
            self.ball.collidesWithPaddle(self.p1_paddle)
            self.ball.collidesWithPaddle(self.p2_paddle)
            
            if 'R' in ball_bounced:        
                self._roundOver(player_win=1)
            elif 'L' in ball_bounced:
                self._roundOver(player_win=2)
                
        ...
        def _roundOver(self, player_win):
            self.paused = True
            if player_win == 1:
                self.p1_score_label.points += 1
            elif player_win == 2:
                self.p2_score_label.points += 2
                
Introduzimos aqui a flag ``paused``, que como o nome diz, nos permite paralizar toda e qualquer movimentação no jogo. Nosso método ``_update()`` também foi reestruturado para incorporar as novas informações. Agora, quando o sprite da bola executa ``run()``, retornará uma string que diz quais limites foram atingidos de acordo com ``bounce()``. Caso ``'R'`` esteja nessa string, então o turno acabou, tendo o player 1 vencido. Caso ``'L'`` esteja nessa string, então o turno acabou com a vitória do player 2. 

O novo método ``_roundOver()`` está bem básico: paraliza-se o jogo e incrementa-se o *score* do player vencedor. O que podemos fazer ainda para acentuar que o *round* acabou? E o que fazemos para dar início a um novo round? Primeiro, poderíamos aproveitar que nossa bola é uma simples superfície e pintá-la, digamos de vermelho, para enfatizar a idéia de "bola fora". Para deixar a coisa mais elegante podemos disponiblizar uma *property* em ``Ball`` para lidar com a cor::

    class Ball(Sprite):
        def __init__(self, diameter, color, **kwargs):
            ...
            self.diameter, self._color = int(diameter), pygame.Color(color)
            ...
            
        @property
        def color(self):
            return self._color
        
        @color.setter
        def color(self, value):
            self._color = pygame.Color(value)
            self.render()
                
        def render(self):
            ...
            surf.fill(self._color)
            ...
        ...
    
Assim, basta fazer ``ball.color = some_new_color``, onde ``some_new_color`` pode ser inclusive uma string válida para ``pygame.Color``, e a bola mudará de cor chamand ``render()`` automaticamente. Também poderiam ter sido criadas *properties* para alterar o diâmetro da bola; isso pode ficar a cargo do leitor.

E como restaurar o jogo, criando um novo round? Primeiro, pode ser interessante que o jogo fique pausado por 1 segundo (mais ou menos) para deixar claro aos jogadores o que aconteceu. Isso significa agendar a execução do método responsável por reiniciar o round, digamos, ``_startNewRound()``. O reinício do round pode se dar da seguinte maneira: a bola, novamente branca, é posicionada na frente do bastão do jogador que marcou o último ponto, e liberada a partir daí na direção do adversário. Isso dará tempo para o adversário reagir e se reposicionar para defender.

A criação de *timers* em Pygame se faz definindo um novo tipo de evento, baseado em ``pygame.USEREVENT``, e usando então ``pygame.time.set_timer()`` nesse novo evento para que o mesmo apareça periodicamente na fila de eventos a serem tratados. Isso nos leva a um novo tipo de tratamento de eventos através do método ``_onUserEvent()``::

    class AppPong(object):        
        ...
        UE_RESTORE_ROUND = pygame.USEREVENT + 1
        TIME_RESTORE_ROUND = 1000    # milliseconds

        def _onUserEvent(self, user_event):
            if user_event == self.UE_RESTORE_ROUND:
                pygame.time.set_timer(user_event, 0)    # desativa o timer
                self._startNewRound()
        
        def execute(self):
            ...
            while True: # --- main loop ---    
                ...            
                for event in pygame.event.get(): # --- tratamento de eventos ---        
                    if event.type == pygame.QUIT:
                        ...
                    else:
                        self._onUserEvent(event.type)
                ...
              
        ...
        def _startNewRound(self):
            pass
            
O timer será disparado dentro de ``_roundOver()``, apartir daonde o jogo estará pausado, até que o evento definido por ``UE_RESTORE_ROUND`` ocorra. Quando isso acontecer, imediatamente desativamos o timer passando um intervalo nulo e chamamos ``_startNewRound()``, a ser implementada como segue::
            
    class AppPong(object):
        ...
        BALL_ATTRIBUTES = ...
        BALL_OUT_COLOR = 'red'
        ...
        def _setup(self):
            ...
            self.last_win = None
            
        ...
        def _roundOver(self, player_win):
            self.paused = True
            self.ball.color = self.BALL_OUT_COLOR
                    
            if player_win == 1:
                self.p1_score_label.points += 1
            elif player_win == 2:
                self.p2_score_label.points += 1
                
            self.last_win = player_win
            pygame.time.set_timer(self.UE_RESTORE_ROUND, self.TIME_RESTORE_ROUND)         
        
        def _startNewRound(self):    
            self.ball.color = self.BALL_ATTRIBUTES['color']
            
            if self.last_win == 1:
                self.ball.pos.x = self.p1_paddle.rect.right
                self.ball.pos.y = self.p1_paddle.pos.y + \
                    (self.p1_paddle.height//2 - self.ball.height//2)
                self.ball.velocity.i = +1
                self.ball.velocity.j = self.p1_paddle.velocity.j
                
            elif self.last_win == 2:
                self.ball.pos.x = self.p2_paddle.pos.x - self.ball.width
                self.ball.pos.y = self.p2_paddle.pos.y + \
                    (self.p2_paddle.height//2 - self.ball.height//2)
                self.ball.velocity.i = -1
                self.ball.velocity.j = self.p2_paddle.velocity.j                
                
            self.paused = False

No novo atributo ``self.last_win`` guardamos o número do player que venceu o último round. Vejamos então o fluxo de acontecimentos: a bola bate em uma das laterais, o round acaba e ``_roundOver()`` é chamada. Ela pinta a bola de vermelho, incrementa a pontuação do jogador que venceu e dispara o timer para o evento ``UE_RESTORE_ROUND``. Dentro de 1 segundo esse evento aparece na lista e ``_startNewRound()`` é chamada. A bola é então pintada de branco de novo, posicionada na frente do bastão correto, com velocidade ajustada, e o jogo é despausado.

Nesse ponto temos toda a mecânica essencial do jogo funcionando OK.