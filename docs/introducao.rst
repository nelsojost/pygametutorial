﻿Introdução
==========

:mod:`1 - Mínimo`
-----------------

Eis o código mínimo para uma aplicação em Pygame iniciar e encerrar corretamente::

    import pygame, sys

    # carrega todos os modulos de pygame (video, som, fontes, etc)
    pygame.init()

    # objeto pygame.Surface que contem o conteudo a ser exibido no monitor
    screen = pygame.display.set_mode((400, 300))

    while True:
        # realiza tratamento de eventos
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()       # desfaz pygame.init() e destroi a janela
                sys.exit()          # encerra o interpretador python

        pygame.display.update()     # atualiza a imagem no monitor

A execução desse código é uma simples janela preta:

.. image:: _static/01-minimal.png
   :align: center

A função ``pygame.display.set_mode()`` recebe como parâmetro uma tupla/lista indicando a resolução da tela, no formato *(largura, altura)*. Ela retorna um objeto ``pygame.Surface`` especial: tudo que for desenhado nessa superfície aparecerá no monitor quando a função ``pygame.display.update()`` for executada - por esse motivo a superfície foi guardada numa variável chamada ``screen``. Quando um objeto ``pygame.Surface`` é criado possui todos os pixels com a cor preta.

Em seguida temos um loop infinito ``while True``, que é conhecido nesse tipo de aplicação como *main loop* - ou até mesmo *game loop*. Três coisas importantes vão acontecer dentro dele (e nessa ordem):

* Tratamento de eventos (pressionamento de teclas, movimento/cliques do mouse, etc);
* Atualizações do estado do jogo (posição dos personagens, scores, etc);
* Desenhos na tela;

Quando um evento acontece, Pygame cria um objeto ``pygame.Event`` que captura as informações do evento e é adicionado a fila corrente. A função ``pygame.event.get()`` retira todos os eventos que estão atualmente nessa fila, retornando-os numa lista. Tratar os eventos significa então percorrer essa lista - é o que faz o loop ``for event in pygame.event.get()``. No caso, estamos tratando apenas a ocorrência do evento ``pygame.QUIT`` - que ocorre, por exemplo, quando o usuário clica no X da janela.

OBS: A função ``sys.exit()`` garante o encerramento do interpretador (e consequentemente, do *main loop*), mas em algums sistemas poder ser que a janela não seja destruída: ``pygame.quit()`` garante que isso aconteça.

:mod:`2 - Objeto Clock`
-----------------------

Há um pequeno problema com o programa apresentado anteriormente: a execução do *main loop* ocorre na velocidade máxima que a CPU é capaz de atingir. Além de ser um disperdício de processamento, o ideal é que o jogo mantenha sua execução em uma taxa de quadros por segundo razoavelmente constante. Isso pode ser alcançado em Pygame com uso do objeto ``pygame.time.Clock``::

    ...
    screen = ...
    clock = pygame.time.Clock()

    while True:
        # mantem execucao em torno de 60 FPS, e retorna
        # o periodo de um quadro (em milisegundos)
        dt = clock.tick(60)
        ...

OBS: Os sinais ``...`` indicam que há código já explicado aqui.

Dessa forma, a cada iteração do *main loop* o método ``tick()`` faz o programa literalmente dormir por uma fração de 1/60 segundo - i.e., podemos afirmar que a duração ou período de um quadro é de aproximadamente 1/60 s = 0.016 s (a precisão de Pygame vai até millisegundos). Em outras palavras, o jogo estará rodando a uma taxa de aproximadamente 60 quadros por segundo (*frames per second*, ou simplesmente, FPS) - valor conhecido como *framerate*, o qual foi passado como parâmetro para ``tick()``.

A função ``tick()`` também retorna o tempo real transcorrido durante um quadro, aqui guardado na variável ``dt``, que será usada logo mais quando falarmos de animações.

:mod:`3 - Superfícies e cores`
----------------------------------------

Os objetos ``pygame.Surface`` representam uma matriz de pixels, onde cada um possui uma cor no formato RGBA (*Red/Green/Blue/Alpha*) - sistema bem comum, usado por diversas bibliotecas gráficas. Em Pygame uma cor pode ser dada simplesmente por uma tupla de 3 inteiros entre 0 e 255, por exemplo: ``(0, 0, 255)`` representa a cor azul pura. Misturando diferentes valores de vermelho, verde e azul, podemos obter até 255³ = 16581375 cores possíveis.

Essas tuplas também podem ter um quarto valor, opcional, chamado *canal alpha*, também entre 0 e 255, que indica a transparência da cor: 0 é totalmente transparente, e 255 (assumido por padrão), totalmente opaco. No entanto, para que os objetos ``pygame.Surface`` suportem cores *alpha* é preciso passar a flag ``SRCALPHA`` na instanciação. O quarto valor, se presente, é meramente ignorado em superfícies não-*alpha* (padrão).

Antes de sair definindo constantes como ``BLACK = (0, 0, 0)``, saiba que Pygame também oferece o objeto ``pygame.Color``, como ilustra o exemplo abaixo::

    import pygame, sys
    from pygame.locals import *

    ...
    while True:
        for event in pygame.event.get():
            ...

        screen.fill(Color('white'))
        ...

.. image:: _static/02-colors.png
   :align: center

onde ``Color('white')`` é o mesmo que ``(255, 255, 255, 255)``. Confira todos os nomes disponíveis no dicionário ``pygame.colordict.THECOLORS``.

Os pixels em uma superfície possuem coordenadas *(x, y)* num sistema que difere um pouco do que se usa na matemática:

* O eixo X cresce da esquerda para a direita;
* O eixo Y cresce de cima para baixo, ao contrário do clássico cartesiano.

A razão disso é o fato dos pixels da superfície estarem dispostos como uma matriz na memória, onde o primeiro elemento - pixel do canto superior esquerdo - tem coordenadas ``(0, 0)``; e o último elemento - pixel do canto inferior direito - tem coordenadas ``(399, 299)`` para o caso de uma resolução de 400 x 300.

O exemplo a seguir ilustra a criação de duas superfíceis (uma normal, e outra *alpha*)::

    ...
    clock = ...

    rect1_surf = pygame.Surface((100, 50))
    rect1_surf.fill((0, 0, 255, 100))   # azul semi-transparente
    rect1_surf.convert()

    rect2_surf = pygame.Surface((100, 50), SRCALPHA)
    rect2_surf.fill((255, 0, 0, 100))   # vermelho semi-transparente
    rect2_surf.convert_alpha()

    while True:
        ...
        screen.fill(Color('white'))
        screen.blit(rect1_surf, (50, 50))
        screen.blit(rect2_surf, (100, 75))
        ...

.. image:: _static/03-surfaces.png
   :align: center

Tal como ``pygame.display.set_mode()``, objetos ``pygame.Surface`` devem receber uma tupla/lista de 2 inteiros no formato *(largura, altura)* para indicar seu tamanho, e opcionalmente, a flag ``SRCALPHA`` para ativar o suporte de cores *alpha*. Veja que foram passadas cores alpha para ambas as superfícies, mas no caso de ``rect1_surf``, a o valor alpha 100 da cor azul ``(0, 0, 255, 100)`` foi simplesmente ignorado. Os métodos ``convert()`` e ``convert_alpha()`` apenas otimizam a superfície para o processo de *blitagem*, descrito a seguir.

Tendo criado as superfícies fora do *main loop*, é preciso desenhá-las em ``screen`` para que apareçam no monitor. Isso foi feito dentro do *main loop*, logo após o fundo ter sido pintado de branco. Objetos ``pygame.Surface`` possuem o método ``blit()``, função bem comum em bibliotecas gráficas. O que ele faz é copiar o conteúdo de uma superfície para aquela de onde ele foi chamado. Em ``screen.blit(rect1_surf, (50, 50))`` todos os pixels de ``rect1_surf`` são copiados para ``screen`` a partir da posição ``(50, 50)``, o canto superior esquerdo de ``rect1_surf``.

Quando a superfície é *alpha* ocorre um processo conhecido como *alpha blending* (mistura alpha): no caso, o vermelho semi-transparente de ``rect2_surf`` é misturado com a cor dos respectivos pixels de ``screen``, resultando em novas cores, como o violeta observado na intersecção. Devido a esse processamento extra, o uso inadequado de superfícies *alpha* pode reduzir o *framerate*.

Finalizamos esta seção observando que Pygame oferece algumas primitivas básicas de desenho no módulo ``pygame.draw``. Por exemplo, um círculo pode ser desenhado como segue::

    while True:
        ...
        screen.blit(...)

        pygame.draw.circle(screen, Color('darkgreen'), (200, 200), 50)
        ...

.. image:: _static/04-draw-circle.png
   :align: center

Como se pode observar, não há *anti-alias*. Em geral, os desenhos de personagens e elementos do jogo são razoavelmente complexos para serem desenhados manualmente em algum editor de imagem. O que se faz então é carregar essas imagens no jogo com ``pygame.image.load()``, que suporta vários formatos comuns como ``.bmp``, ``.png`` e ``.jpg``.

Entretanto, note que não é boa prática usar as primitivas de desenho diretamente no *main loop*. Primeiro, porque não há necessidade de recriar a mesma figura, caso ela não tenha mudado: para isso existem os objetos ``pygame.Surface``. Segundo, essas primitivas, como ``pygame.draw.circle()``, por exemplo, aceitam apenas coordenadas de números inteiros - ao contrário de ``blit()``, que aceita valores ``float``, algo importante para animações como veremos na próxima seção.

Levando em conta esses fatores, considere a seguinte mudança::

    ...
    ball_surf = pygame.Surface((100, 100))
    pygame.draw.circle(ball_surf, Color('darkgreen'), (50, 50), 50)
    ball_surf.convert()

    while True:
        ...
        screen.blit(ball_surf, (150, 150))
        ...

.. image:: _static/05-draw-circle-surf.png
   :align: center

Queremos uma bola de raio 50, por isso criamos uma superfície quadrada de lado 100. Utilizamos então ``pygame.draw.circle()`` para desenhar agora em ``ball_surf`` - observe que a coordenada ``(50, 50)`` é relativa a esta superfície, correspondendo exatamente ao centro da mesma. Lembre também que a coordenada passada para ``blit()`` corresponde ao canto superior esquerdo da superfície a ser "blitada", aqui ``ball_surf``, em relação à ``screen``. Foi escolhido ``(150, 150)`` para que a bola ficasse na mesma posição de anteriormente, quando foi passada a coordenada ``(200, 200)`` como centro do círculo.

Naturalmente, observa-se que o fundo da superfície ``ball_surf`` ficou preto. Sempre que um objeto ``pygame.Surface`` é criado, seus pixels possuem a cor preta ``(0, 0, 0, 255)``. Poderíamos fazer o fundo ficar transparente usando uma superfície *alpha* e pintando-o com, digamos, ``(0, 0, 0, 0)``. Porém, ilustrarei aqui um método alternativo. Como já sabemos de antemão que essa cor preta não será usada na figura, basta configurá-la como *colorkey* e ela será ignorada na *blitagem*::

    ...
    ball_surf = pygame.Surface(...)
    ball_surf.set_colorkey(ball_surf.get_at((0, 0)))
    pygame.draw.circle(...)
    ...

.. image:: _static/04-draw-circle.png
   :align: center

onde a função ``get_at()`` retorna a cor de um certo pixel da superfície. Como ela foi chamada imediatamente depois da criação da superfície, sabemos que qualquer pixel terá a cor preta que deseja-se transformar em *colorkey*.

Caso fosse desejado utilizar alguma outra cor como *colorkey*, digamos que definida numa constante ``MY_COLORKEY``, bastaria pintar a superfície com essa cor imediatamente após a criação, como segue::

    ...
    ball_surf = pygame.Surface(...)
    ball_surf.fill(MY_COLORKEY)
    ball_surf.set_colorkey(ball_surf.get_at((0, 0)))
    pygame.draw.circle(...)
    ...


:mod:`4 - Animações: o básico`
------------------------------

Considere o seguinte código como ponto de partida::

    from __future__ import division, print_function
    import pygame, sys
    from pygame.locals import *

    # propriedades gerais do programa
    RESOLUTION, FRAMERATE = (400, 300), 60
    BACKGROUND_COLOR = Color('white')

    # propriedades do objeto a ser movido
    BALL_RADIUS, BALL_COLOR = 25, Color('blue')
    BALL_POS_0 = (50, 50)       # posicao inicial

    # --- inicializacao --------------------------------------------------

    pygame.init()
    screen = pygame.display.set_mode(RESOLUTION)
    clock = pygame.time.Clock()

    # possui uma superficie quadrada de lado 2*BALL_RADIUS
    ball = {'surf': pygame.Surface(2*[2*BALL_RADIUS]),
            'pos': list(BALL_POS_0)}

    ball['surf'].fill(BACKGROUND_COLOR)
    ball['surf'].set_colorkey(BACKGROUND_COLOR)
    pygame.draw.circle(ball['surf'], BALL_COLOR,
                       ball['surf'].get_rect().center, BALL_RADIUS)
    ball['surf'].convert()

    # --- MAIN LOOP -------------------------------------------------------

    while True:
        time_frame = clock.tick(FRAMERATE)

        # --- tratamento de eventos ---------------------------------------
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        # --- desenhos ----------------------------------------------------
        screen.fill(BACKGROUND_COLOR)
        screen.blit(ball['surf'], ball['pos'])

        pygame.display.update()

.. image:: _static/05-animations-start.png
   :align: center

A primeira linha, ``from __future__ import ...`` está aí por questôes de compatibilidade com Python 2.x, pois o código está sendo escrito assumindo a sintaxe de Python 3.x (em particular sobre as divisões). Além disso, também definimos na parte superior algumas constantes de caráter global, ali agrupadas para facilitar a manutenção.

Logo mais temos a criação do dicionário ``ball``, que captura as informações sobre o objeto que estaremos fazendo se mover. No atributo ``'surf'`` temos uma superfície quadrada de lado ``2*BALL_RADIUS``, e no atributo ``'pos'`` temos a posição da bola, incializada com o valor de ``BALL_POS_0`` convertido para uma lista - naturalmente, porque desejamos que ela mude. Optamos por fazer assim também

O desenho da superfície da bola segue usando as técnicas descritas na seção anterior. Chamamos a atenção aqui para o comando ``ball['surf'].get_rect().center``, onde o método ``get_rect()`` dos objetos ``pygame.Surface`` retorna um objeto ``pygame.Rect``, ainda não discutido. Esse objeto basicamente guarda e manipula os atributos de um retângulo, como as coordenadas dos cantos e suas dimensões. O atributo ``center``, como o nome já diz, retorna o ponto central do retângulo. Falaremos mais sobre esse objeto mais adiante, mas aqui poderia ter sido feito, alternativamente, ``2*[BALL_RADIUS//2]`` para se obter a coordenada do centro do círculo.

Pois bem, temos um cenário mínimo pronto para experimentarmos animações. A forma mais imediata que poderia-se pensar é fazer::

    ...
    while True:
        # --- tratamento de eventos ---------------------------------------
        ...

        # --- atualizacoes do estado --------------------------------------
        ball['pos'][0] += 2

        # --- desenhos ----------------------------------------------------
        ...

onde foi inaugurada uma nova área de código dentro do *main loop*, destinada a realizar alterações no estado dos objetos do jogo. Nesse caso, a posição da bola está sendo incrementada em 2 unidades a cada iteração do *main loop*. Qual a velocidade da bola, em pixels/segundo? Vejamos: se o *main loop* está sendo executado a uma taxa de aproximadamente 60 quadros por segundo, e a bola avança 2 pixel em cada quadro, então a velocidade da bola é aproximadamente 2 x 60 = 120 pixels por segundo.

Essa forma de se fazer animação usando-se deslocamentos absolutos, independentes do período do quadro, é conhecida como animação *frame-based*. Trata-se de uma má prática de programação, já que mudando-se o *framerate*, muda-se a velocidade da animação. Dificilmente o *framerate* de um jogo se manterá constante, e mesmo nas melhores CPU, serão observadas flutuações significativas nos momentos de intenso processamento (quedas de 60 para 50 FPS, por exemplo). Isso deveria afetar apenas a fluidez da animação, e não a posição do objeto em função do tempo.

Seja a velocidade da bola dada por ``ball['speed'] = [100, 50]``, onde definimos as velocidades de 100 px/s na direção X e 50 px/s na direção Y. Sabendo que o tempo real passado entre os quadros é retornado por ``clock.tick()``, e está na variável ``time_frame``, podemos calcular os deslocamentos usando física básica (distância = velocidade x tempo)::

    ...
    BALL_POS_0, BALL_SPEED_0 = (50, 50), (100, 50)
    ...
    ball = {'surf': pygame.Surface(2*[2*BALL_RADIUS]),
            'pos': list(BALL_POS_0), 'speed': list(BALL_SPEED_0)}

    while True:
        ...
        # --- atualizacoes do estado --------------------------------------
        dt = time_frame/1000.
        ball['pos'][0] += ball['speed'][0] * dt
        ball['pos'][1] += ball['speed'][1] * dt
        ...

E assim podemos observar a bola se movendo tanto na direção X como na direção Y numa animação que independe do *framerate*, no que diz respeito à posição do objeto em função do tempo. Isso é conhecido como animação *time based*. Você pode aumentar o *framerate* para ter uma animação mais suave, mas isso não afetará o movimento do objeto em si.
